const mongoose = require('mongoose');
require('dotenv').config();

const MONGO_URL =
  process.env.MONGO_URI ||
  'mongodb+srv://andriibilyanskii:MongoDB_Andry_EPAM@todo.tp8sd.mongodb.net/todo';
mongoose.connect(MONGO_URL, {
  useNewUrlParser: true,
  // useCreateIndex: true,
  // useFindAndModify: false,
  useUnifiedTopology: true,
});
