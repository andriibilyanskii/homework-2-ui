const bcrypt = require('bcrypt');

/**
 * @constructor
 * @param {string} reqPassword - request
 * @param {string} userPassword - response
 */
function comparePassword(reqPassword, userPassword) {
  return bcrypt.compareSync(reqPassword, userPassword);
}

module.exports = comparePassword;
