const jwt = require('jsonwebtoken');
const {errorObj} = require('../model/error');

/**
 * @constructor
 * @param {string} req - request
 * @param {string} res - response
 * @param {string} next - next
 */
function authorization(req, res, next) {
  const header = req.headers.authorization || '';
  const token = header.replace(/JWT\s|Bearer\s/gi, '');
  if (!token) {
    return res.status(400).send(errorObj);
  }
  try {
    const decoded = jwt.verify(token, process.env.TOKEN_KEY || 'secretkey');
    req.user = decoded;
  } catch (err) {
    return res.status(400).send(errorObj);
  }
  next();
}
module.exports = authorization;
