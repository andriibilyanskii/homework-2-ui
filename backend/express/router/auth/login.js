const {errorObj} = require('../../model/error');
const {successObj} = require('../../model/success');
const comparePassword = require('../../../shared/comparePassword');
const jwt = require('jsonwebtoken');
const Credentials = require('../../model/credentials');

/**
 * @constructor
 * @param {string} req - request
 * @param {string} res - response
 */
async function login(req, res) {
  try {
    const {username, password} = req.body;

    if (!(username && password)) {
      res.status(400).send(errorObj);
    }

    const user = await Credentials.findOne({username: username});
    const comparedPassword = comparePassword(password, user.password);
    if (user && comparedPassword) {
      const token = jwt.sign(
          {
            user_id: user._id,
            username: username,
            password: password,
          },
          process.env.TOKEN_KEY || 'secretkey',
          {
            expiresIn: '2h',
          },
      );
      user.token = token;
      res.status(200).send({
        ...successObj,
        jwt_token: token,
      });
    } else {
      res.status(400).send(errorObj);
    }
  } catch (e) {
    res.status(500).send(errorObj);
  }
}

module.exports = login;
