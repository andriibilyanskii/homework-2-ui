const Users = require('../../model/users');
const {errorObj} = require('../../model/error');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const Credentials = require('../../model/credentials');
const {successObj} = require('../../model/success');

/**
 * @constructor
 * @param {string} req - request
 * @param {string} res - response
 */
async function register(req, res) {
  try {
    const {username, password} = req.body;

    const users = await Users.find({username: username});
    if (users.length === 0) {
      const encryptedPassword = await bcrypt.hash(password, 10);
      const token = jwt.sign(
          {
            username: username,
            password: password,
          },
          process.env.TOKEN_KEY || 'secretkey',
          {
            expiresIn: '2h',
          },
      );

      const credential = new Credentials({
        username,
        password: encryptedPassword,
        token: token,
      });
      const user = new Users({
        username,
        createdDate: new Date().toISOString(),
      });
      await credential.save();
      await user.save().then(() => res.status(200).send(successObj));
    } else {
      res.status(400).send(errorObj);
    }
  } catch (e) {
    console.log(e)
    res.status(500).send(errorObj);
  }
}

module.exports = register;
