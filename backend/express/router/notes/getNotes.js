const Notes = require('../../model/notes');
const {errorObj} = require('../../model/error');
const getNotesInfo = require('./getNotesInfo');

/**
 * @constructor
 * @param {string} req - request
 * @param {string} res - response
 */
async function getNotes(req, res) {
  const offset = +req.query.offset || 0;
  const limit = +req.query.limit || 0;
  try {
    const notes = await Notes.find({userId: req.user.user_id})
        .skip(offset)
        .limit(limit);
    const count = notes.length;
    if (count === 0) {
      res.status(400).send(errorObj);
    } else {
      res.status(200).send({
        offset: offset,
        limit: limit,
        count,
        notes: getNotesInfo(notes),
      });
    }
  } catch (e) {
    res.status(500).send(errorObj);
  }
}

module.exports = getNotes;
