const Notes = require('../../model/notes');
const {errorObj} = require('../../model/error');
const {successObj} = require('../../model/success');

/**
 * @constructor
 * @param {string} req - request
 * @param {string} res - response
 */
async function createNote(req, res) {
  try {
    const {text} = req.body;

    const notes = await Notes.find({text: text, userId: req.user.user_id});
    if (notes.length === 0) {
      const note = new Notes({
        userId: req.user.user_id,
        completed: false,
        text,
        createdDate: new Date().toISOString(),
      });
      await note.save().then(() => res.status(200).send(successObj));
    } else {
      res.status(400).send(errorObj);
    }
  } catch (e) {
    res.status(500).send(errorObj);
  }
}

module.exports = createNote;
