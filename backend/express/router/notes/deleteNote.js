const Notes = require('../../model/notes');
const {errorObj} = require('../../model/error');
const {successObj} = require('../../model/success');

/**
 * @constructor
 * @param {string} req - request
 * @param {string} res - response
 */
async function deleteNote(req, res) {
  try {
    const id = req.params.id;

    const note = await Notes.findOne({_id: id, userId: req.user.user_id});
    if (note) {
      await Notes.deleteOne({_id: id});
      res.status(200).send(successObj);
    } else {
      res.status(400).send(errorObj);
    }
  } catch (e) {
    res.status(500).send(errorObj);
  }
}

module.exports = deleteNote;
