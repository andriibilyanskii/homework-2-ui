const Notes = require('../../model/notes');
const {errorObj} = require('../../model/error');
const getNotesInfo = require('./getNotesInfo');

/**
 * @constructor
 * @param {string} req - request
 * @param {string} res - response
 */
async function getNote(req, res) {
  const id = req.params.id;
  try {
    const notes = await Notes.find({_id: id, userId: req.user.user_id});
    if (notes.length === 0) {
      res.status(400).send(errorObj);
    } else {
      res.status(200).send({note: getNotesInfo(notes)[0]});
    }
  } catch (e) {
    res.status(500).send(errorObj);
  }
}

module.exports = getNote;
