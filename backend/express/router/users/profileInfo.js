const Users = require('../../model/users');
const {errorObj} = require('../../model/error');

/**
 * @constructor
 * @param {string} req - request
 * @param {string} res - response
 */
async function profileInfo(req, res) {
  try {
    const user = await Users.findOne({username: req.user.username});
    if (user) {
      res.status(200).send({
        _id: user._id,
        username: user.username,
        createdDate: user.createdDate,
      });
    } else {
      res.status(400).send(errorObj);
    }
  } catch (e) {
    res.status(500).send(errorObj);
  }
}

module.exports = profileInfo;
