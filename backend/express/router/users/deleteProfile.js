const Users = require('../../model/users');
const {errorObj} = require('../../model/error');
const {successObj} = require('../../model/success');
const Credentials = require('../../model/credentials');

/**
 * @constructor
 * @param {string} req - request
 * @param {string} res - response
 */
async function deleteProfile(req, res) {
  try {
    const user = await Users.findOne({username: req.user.username});
    if (user) {
      await Credentials.deleteOne({username: req.user.username});
      await Users.deleteOne({username: req.user.username});
      res.status(200).send(successObj);
    } else {
      res.status(400).send(errorObj);
    }
  } catch (e) {
    res.status(500).send(errorObj);
  }
}

module.exports = deleteProfile;
