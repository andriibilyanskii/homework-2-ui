const express = require('express');
const login = require('./auth/login');
const register = require('./auth/register');
const router = new express.Router();

router.post('/api/auth/register', (req, res) => {
  register(req, res);
});

router.post('/api/auth/login', (req, res) => {
  login(req, res);
});

module.exports = router;
