const express = require('express');
const changeState = require('./notes/changeState');
const router = new express.Router();
const createNote = require('./notes/createNote');
const deleteNote = require('./notes/deleteNote');
const getNote = require('./notes/getNote');
const getNotes = require('./notes/getNotes');
const updateNote = require('./notes/updateNote');

router.get('/api/notes', (req, res) => {
  getNotes(req, res);
});

router.post('/api/notes', (req, res) => {
  createNote(req, res);
});

router.get('/api/notes/:id', (req, res) => {
  getNote(req, res);
});

router.put('/api/notes/:id', (req, res) => {
  updateNote(req, res);
});

router.patch('/api/notes/:id', (req, res) => {
  changeState(req, res);
});

router.delete('/api/notes/:id', (req, res) => {
  deleteNote(req, res);
});

module.exports = router;
