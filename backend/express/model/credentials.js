const mongoose = require('mongoose');

const credentialsSchema = new mongoose.Schema({
  username: {
    type: String,
    require: true,
    trim: true,
  },
  password: {
    type: String,
    require: true,
    trim: false,
  },
  token: {
    type: String,
    trim: true,
  },
});

const Credentials = mongoose.model('Credentials', credentialsSchema);

module.exports = Credentials;
