const mongoose = require('mongoose');

const usersSchema = new mongoose.Schema({
  username: {
    type: String,
    require: true,
    trim: true,
  },
  createdDate: {
    type: String,
    require: true,
    trim: true,
  },
});

const Users = mongoose.model('User', usersSchema);

module.exports = Users;
