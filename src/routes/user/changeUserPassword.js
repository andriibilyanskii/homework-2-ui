export default function changeUserPassword(state) {
  fetch('http://localhost:5000/api/users/me', {
    method: 'PATCH',
    headers: {
      'Content-Type': 'application/json;charset=utf-8',
      Authorization: `Bearer ${state.user.jwt_token}`,
    },
    body: JSON.stringify({
      oldPassword: state.oldpassword,
      newPassword: state.newpassword,
    }),
  })
    .then((res) => res.json())
    .then(() => {
      window.localStorage.removeItem('user');
      window.location.pathname = '';
    })
    .catch((error) => console.log(error));
}
