export default function deleteUserProfile(jwt_token) {
  fetch('http://localhost:5000/api/users/me', {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json;charset=utf-8',
      Authorization: `Bearer ${jwt_token}`,
    },
  })
    .then((res) => res.json())
    .then(() => {
      window.localStorage.removeItem('user');
      window.location.pathname = '';
    })
    .catch((error) => console.log(error));
}
