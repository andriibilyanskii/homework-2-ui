import loginUser from './login';

export default function registerUser(state) {
    fetch('http://localhost:5000/api/auth/register', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json;charset=utf-8',
      },
      body: JSON.stringify({
        username: state.username,
        password: state.password,
      }),
    })
      .then((res) => res.ok)
      .then((data) => {
        if (data) {
          loginUser(state);
        } else {
          alert('This account is already created');
          window.location.reload();
        }
      })
      .catch((error) => console.log(error));
}