export default function loginUser(state) {
  let jwt_token = '';
  fetch('http://localhost:5000/api/auth/login', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json;charset=utf-8',
    },
    body: JSON.stringify({
      username: state.username,
      password: state.password,
    }),
  })
    .then((res) => res.json())
    .then((data) => {
      jwt_token = data.jwt_token;
      fetch('http://localhost:5000/api/users/me', {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json;charset=utf-8',
          Authorization: `Bearer ${jwt_token}`,
        },
      })
        .then((res) => res.json())
        .then((data) => {
          if (jwt_token) {
            window.localStorage.setItem(
              'user',
              JSON.stringify({
                username: state.username,
                jwt_token: jwt_token,
                userId: data._id,
              }),
            );
            window.location.pathname = '/notes';
          }
        })
        .catch((error) => console.log(error));
    })
    .catch((error) => console.log(error));
}
