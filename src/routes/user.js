import React, {Component} from 'react';
import Exit from '../shared/exitButton';
import formatDate from './../shared/formatDate';
import getUserInfo from './user/getUserInfo';
import deleteUserProfile from './user/deleteUserProfile';
import changeUserPassword from './user/changeUserPassword';

class User extends Component {
  constructor(props) {
    super(props);
    this.state = {
      oldpassword: '',
      newpassword: '',
      user: localStorage.getItem('user')
        ? JSON.parse(localStorage.getItem('user'))
        : {username: '', jwt_token: ''},
    };

    this.getProfileInfo = this.getProfileInfo.bind(this);
    this.changePassword = this.changePassword.bind(this);
    this.changeInput = this.changeInput.bind(this);
  }

  componentDidMount() {
    this.getProfileInfo();
  }

  getProfileInfo() {
    getUserInfo(this.state.user.jwt_token);
    
    this.setState({
      user: localStorage.getItem('user')
        ? JSON.parse(localStorage.getItem('user'))
        : {username: '', jwt_token: ''},
    });
  }

  deleteProfile() {
    deleteUserProfile(this.state.user.jwt_token);
  }

  changePassword(e) {
    e.preventDefault();

    changeUserPassword(this.state);
    this.getProfileInfo();
    this.setState({oldpassword: '', newpassword: ''});
  }

  changeInput(e) {
    this.setState({[e.target.name]: e.target.value});
  }

  render() {
    const e = this.state.user || {};
    return (
      <div>
        <button
          onClick={() => (window.location.pathname = '/notes')}
          className="btn btn-primary"
        >
          Notes
        </button>
        <Exit />
        <div className="profileinfo">
          <table>
            <thead>
              <tr>
                <td>Username</td>
                <td>Creation date</td>
                <td></td>
              </tr>
            </thead>
            <tbody>
              <tr key={e._id}>
                <td className="username">{e.username}</td>
                <td>{formatDate(new Date(e.createdDate))}</td>
                <td>
                  <button
                    onClick={() => this.deleteProfile(e._id)}
                    className="btn btn-danger"
                  >
                    Delete profile
                  </button>
                </td>
              </tr>
            </tbody>
          </table>
          <div className="change-password">
            <p>Change password</p>
            <form onSubmit={this.changePassword}>
              <input
                type="password"
                placeholder="Old password"
                name="oldpassword"
                value={this.state.oldpassword}
                onChange={this.changeInput}
              />
              <input
                type="password"
                placeholder="New password"
                name="newpassword"
                value={this.state.newpassword}
                onChange={this.changeInput}
              />
              <button type="submit" className="btn btn-warning">
                Change password
              </button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default User;
